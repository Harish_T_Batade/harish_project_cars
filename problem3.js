// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
// Execute a function to Sort all the car model names into alphabetical order and log 
//the results in the console as it was returned.

function sortmodelname(inventory)
{
    return inventory.sort(function(a, b){
        let x = a.car_model.toLowerCase();
        let y = b.car_model.toLowerCase();
        if (x < y) {return -1;}
        if (x > y) {return 1;}
        return 0;
      });
}

module.exports = sortmodelname;